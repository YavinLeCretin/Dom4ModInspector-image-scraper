from selenium import webdriver
from selenium.webdriver.common.by import By

import time
from PIL import Image

class dom4scraper(object):
   def __init__(self):
       self.driver = webdriver.PhantomJS('/Users/kacper/Downloads/phantomjs-2.1.1-macosx/bin/phantomjs')
       self.driver.set_window_size(1120, 550)

   def scrap(self, adress, output):
       '''
       method to download image & data from dom4mod inspector and store them in data.py
       :param adress: adress of the page
       :param output: output adress
       :return:
       '''
       self.driver.get(adress)
       time.sleep(5)
       UnitDiv = self.driver.find_element(By.XPATH, "//div[@class='unit overlay-contents']")
       UnitDiv = UnitDiv.find_element_by_xpath('..')
       loc1 = UnitDiv.location
       size1 = UnitDiv.size
       self.driver.save_screenshot(output)
       self.driver.quit()

       OutputImage = Image.open(output)
       left, top, right, bottom = loc1['x'], loc1['y'], loc1['x'] + size1['width'], loc1['y'] + size1['height']
       OutputImage = OutputImage.crop((left, top, right, bottom))
       OutputImage.save(output)


if __name__ == '__main__':
   scrappy = dom4scraper()
   address = 'http://larzm42.github.io/dom4inspector/?page=unit&unitq=vine'
   scrappy.scrap(address, 'dupa.png')